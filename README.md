splay
=====

C++ implementation of Splay tree. Interface similar to the std::set. This code implements concept of Container and implements BidirectionalIterator.

Building project
----------------

To build project:

```sh
mkdir build; cd build
cmake ..
make
```

Running tests
-------------

Just simply write

```sh
make test
```

to run tests. 

If you would like to see more detailed output run `runtests` program.

To check test coverage run `make coverage` and open `coverage/index.html` file.