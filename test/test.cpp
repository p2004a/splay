#include <random>
#include <utility>
#include <vector>
#include <set>
#include <algorithm>

#include "gtest/gtest.h"
#include "splay.h"

TEST(SplayTest, Constructor) {
    splay<int> a, b({123123});
    for (int i = 0; i < 10; ++i) {
        a.insert(i);
    }
    a.erase(5);
    b = a;
    splay<int> d(b);

    EXPECT_TRUE(a == b);
    EXPECT_TRUE(b == d);
    EXPECT_TRUE(a == d);

    d.erase(1);
    b.erase(2);

    EXPECT_TRUE(a != b);
    EXPECT_TRUE(b != d);
    EXPECT_TRUE(a != d);

    splay<int> c({101, 102, 103, 104});
    splay<int>::iterator it = c.begin();
    for (int i = 1; i <= 4; ++i, ++it) {
        EXPECT_EQ(i + 100, *it);
    }

    std::vector<int> v({1,2,3,4,5,6,7,8,9});
    {
        splay<int> e;
        e.insert(v.begin(), v.end());
        e.erase(4);
        e.insert(5);
        e.insert(4);

        splay<int> f(std::move(e));
        f.erase(4);
        f.insert(5);
        f.insert(4);

        d = std::move(f);
    }
    d.erase(4);
    d.insert(5);
    d.insert(4);
    EXPECT_TRUE(std::equal(v.begin(), v.end(), d.begin()));
    EXPECT_EQ(d.size(), 9);
}

TEST(SplayTest, Insert) {
    splay<int, std::greater<int>> s;
    s.insert({0,1,6,7,4});
    std::vector<int> v({5,2,3});
    s.insert(v.begin(), v.end());
    s.insert(8);
    s.insert(8);
    s.insert(9);
    s.insert({2,5,6});
    s.insert(v.begin(), v.end());

    std::vector<int> res({9,8,7,6,5,4,3,2,1,0});

    EXPECT_TRUE(std::equal(res.begin(), res.end(), s.begin()));
    EXPECT_EQ(s.size(), 10);
}

TEST(SplayTest, Erase) {
    splay<int> s({9,8,7,6,5,4,3,2,1,0});
    s.erase(5);
    s.erase(2);
    s.erase(3);
    s.erase(0);

    std::set<int> res1({9,8,7,6,4,1});
    EXPECT_TRUE(std::equal(res1.begin(), res1.end(), s.begin()));

    s.erase(s.lower_bound(0), s.upper_bound(7));

    s.insert(1);

    splay<int> res2({9,8,1});
    EXPECT_TRUE(res2 == s);
    EXPECT_EQ(s.size(), 3);

    s.erase(100);

    while (!s.empty()) {
        s.erase(s.begin());
    }
    s.erase(s.begin());
    s.erase(0);

    EXPECT_EQ(s.begin(), s.end());
}

TEST(SplayTest, Find) {
    splay<int> s({1,3,5,7,9,20,1000,50,70,13});

    EXPECT_NE(s.find(3), s.end());
    EXPECT_NE(s.find(9), s.end());
    EXPECT_NE(s.find(1), s.end());
    EXPECT_EQ(s.find(100), s.end());
    EXPECT_EQ(s.find(-2), s.end());
    EXPECT_EQ(s.find(6), s.end());
}

TEST(SplayTest, LowerUpperBound) {
    splay<int> s({1,3,5,7,9,20,1000,50,70,13});

    EXPECT_EQ(s.lower_bound(3), s.find(3));
    EXPECT_EQ(s.lower_bound(9), s.find(9));
    EXPECT_EQ(s.lower_bound(120), s.find(1000));
    EXPECT_EQ(s.lower_bound(19), s.find(20));

    EXPECT_EQ(s.upper_bound(3), s.find(5));
    EXPECT_EQ(s.upper_bound(9), s.find(13));
    EXPECT_EQ(s.upper_bound(120), s.find(1000));
    EXPECT_EQ(s.upper_bound(19), s.find(20));

    EXPECT_EQ(s.lower_bound(1001), s.end());
    s.clear();
    EXPECT_EQ(s.lower_bound(1), s.end());
}

TEST(SplayTest, EqualRange) {
    splay<int> s({1,3,5,7,9,20,1000,50,70,13});
    auto res = s.equal_range(5);
    EXPECT_EQ(res.first, s.find(5));
    EXPECT_EQ(res.second, s.find(7));
}

TEST(SplayTest, RandomEraseInsertFind) {
    using namespace std;

    random_device rd;
    default_random_engine rengine(rd());
    uniform_int_distribution<int> random_number(1, 50);
    uniform_int_distribution<int> random_operation(1, 3);

    set<int> s1;
    splay<int> s2;

    for (int i = 0; i < 20000; ++i) {
        int num = random_number(rengine);
        switch (random_operation(rengine)) {
            case 1: {
                auto res1 = s1.insert(num);
                auto res2 = s2.insert(num);
                EXPECT_EQ(res1.second, res2.second);
                break;
            }

            case 2: {
                auto res1 = s1.erase(num);
                auto res2 = s2.erase(num);
                EXPECT_EQ(res1, res2);
                break;
            }

            case 3: {
                auto res1 = s1.find(num);
                auto res2 = s2.find(num);
                EXPECT_EQ(res1 == s1.end(), res2 == s2.end());
                break;
            }
        }
    }

    EXPECT_EQ(s1.size(), s2.size());
    EXPECT_TRUE(equal(s1.begin(), s1.end(), s2.begin()));

    s1.clear();
    s2.clear();
}

TEST(SplayTest, Iterator) {
    splay<int> s;
    auto it = s.begin();

    EXPECT_EQ(it, s.end());
    ++it;
    EXPECT_EQ(it, s.end());
    --it;
    EXPECT_EQ(it, s.end());

    std::vector<int> in({1,2,3,4,5});
    s.insert(in.begin(), in.end());
    s.find(4);
    {
        int i = 0;
        for (int a: s) {
            EXPECT_EQ(a, in[i]);
            ++i;
        }
    }

    EXPECT_EQ(it, s.end());
    ++it;
    EXPECT_EQ(it, s.end());
    --it;
    EXPECT_NE(it, s.end());
    EXPECT_EQ(*it, in.back());

    it = s.begin();
    auto it2 = it++;
    EXPECT_EQ(*it2, in.front());
    ++it2;
    EXPECT_EQ(it2, it);

    std::advance(it, 3);
    splay<int>::iterator it3;
    it3 = it--;
    it3--;
    EXPECT_EQ(it3, it);
    it3 = it;
    ++it3;
    ++it;
    EXPECT_EQ(it3, it);

    EXPECT_EQ(std::distance(s.begin(), s.end()), s.size());

    splay<std::pair<int, int>> d;
    d.insert(std::make_pair(2, 5));
    auto dit = d.begin();
    EXPECT_EQ(dit->first, 2);
    EXPECT_EQ(dit->second, 5);
}

TEST(SplayTest, MaxSize) {
    EXPECT_GE(splay<int>::max_size(), 100000);
}
