/*
    Copyright 2014 Marek "p2004a" Rusinowski
*/
#pragma once

#include <algorithm>
#include <iterator>
#include <limits>
#include <utility>
#include <functional>

#define left s[0]
#define right s[1]

template <class T, class Compare = std::less<T>>
class splay {
  public:
    typedef T value_type;
    typedef T& reference;
    typedef const T& const_reference;
    class const_iterator;
    // C++ Standard Library Issue 103. set::iterator is required to be modifiable, but this allows modification of keys
    typedef const_iterator iterator;
    typedef typename iterator::difference_type difference_type;
    typedef unsigned long size_type;

  private:
    class splay_node_base;
    class splay_node;
    splay_node_base header;
    size_type num_elements;

    class splay_node_comparator;
    splay_node_comparator comp;

    void initialize_header() {
        header.parent = nullptr;
        header.left = header.right = &header;
    }

    void set_root(splay_node_base *node) {
        header.parent = node;
        node->parent = &header;
    }

    constexpr splay_node_base *root() const {
        return header.parent;
    }

  public:
    splay(const Compare& comp_ = Compare()) : num_elements(0), comp(comp_) {
        initialize_header();
    }

    splay(std::initializer_list<value_type> il, const Compare& comp_ = Compare()) : splay(comp_) {
        initialize_header();
        insert(il);
    }

    splay(splay && other) : header(other.header), num_elements(other.num_elements), comp(other.comp) {
        header.right = &header;
        set_root(header.parent);

        other.header.parent = nullptr;
        other.num_elements = 0;
    }

    splay & operator=(splay && other) {
        swap(other);
        return *this;
    }

    splay(splay const& other) : num_elements(0), comp(other.comp) {
        initialize_header();
        if (!other.empty()) {
            set_root(new splay_node(other.root()));
            header.left = root()->minimal();
            num_elements = other.num_elements;
        }
    }

    splay & operator=(splay const& other) {
        if (!empty()) {
            clear();
        }
        initialize_header();
        num_elements = 0;
        comp = other.comp;
        if (!other.empty()) {
            set_root(new splay_node(other.root()));
            header.left = root()->minimal();
            num_elements = other.num_elements;
        }
        return *this;
    }

    ~splay() {
        clear();
    }

    void swap(splay<T, Compare>& other) {
        std::swap(comp, other.comp);
        std::swap(header, other.header);
        std::swap(num_elements, other.num_elements);

        other.header.right = &other.header;
        other.set_root(other.header.parent);

        header.right = &header;
        set_root(header.parent);
    }

    size_type size() const {
        return num_elements;
    }

    static constexpr size_type max_size() {
        return std::numeric_limits<size_type>::max();
    }

    bool empty() const {
        return root() == nullptr;
    }

    iterator begin() {
        return cbegin();
    }

    const_iterator begin() const {
        return cbegin();
    }

    const_iterator cbegin() const {
        if (empty()) {
            return end();
        }
        return const_iterator(header.left);
    }

    iterator end() {
        return cend();
    }

    const_iterator end() const {
        return cend();
    }

    const_iterator cend() const {
        return const_iterator(const_cast<splay_node_base*>(&header));
    }

    bool operator==(const splay<T, Compare>& other) const {
        if (size() != other.size()) {
            return false;
        }
        return std::equal(begin(), end(), other.begin());
    }

    bool operator!=(const splay<T, Compare>& other) const {
        return !operator==(other);
    }

    void clear() {
        if (!empty()) {
            delete root();
        }
        initialize_header();
        num_elements = 0;
    }

  private:
    splay_node_base *insert(splay_node_base *node, const T &value, bool *inserted = nullptr) {
        if (comp.equiv(node, value)) {
            if (*inserted) {
                *inserted = false;
            }
            return node->splay();
        } else {
            bool dir = comp(node, value);
            if (node->s[dir]) {
                return insert(node->s[dir], value, inserted);
            } else {
                node->s[dir] = new splay_node(value, node);
                if (*inserted) {
                    *inserted = true;
                }
                return node->s[dir]->splay();
            }
        }
    }

    splay_node_base *find(splay_node_base *node, const T &value) {
        if (comp(value, node) && node->left) {
            return find(node->left, value);
        } else if (comp(node, value) && node->right) {
            return find(node->right, value);
        } else {
            return node->splay();
        }
    }

  public:
    std::pair<iterator, bool> insert(const T &value) {
        bool inserted = true;
        if (empty()) {
            set_root(new splay_node(value, nullptr));
        } else {
            insert(root(), value, &inserted);
        }
        if (inserted) {
            ++num_elements;
            if (size() == 1) {
                header.left = root();
            }
            header.left = root()->minimal();
        }
        return std::make_pair(iterator(root()), inserted);
    }

    template<class InputIt>
    void insert(InputIt first, InputIt last) {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    void insert(std::initializer_list<value_type> ilist) {
        insert(ilist.begin(), ilist.end());
    }

    iterator erase(const_iterator pos) {
        if (pos == end()) {
            return end();
        }

        pos.node->splay();
        ++pos;

        splay_node_base *tmp = root();
        if (size() == 1) {
            initialize_header();
        } else {
            if (root()->left == nullptr) {
                set_root(root()->right);
            } else if (root()->right == nullptr) {
                set_root(root()->left);
                            } else {
                set_root(root()->left);
                while (root()->right != nullptr) {
                                    root()->right->rotate();
                }
                root()->right = tmp->right;
                root()->right->parent = root();
            }
        }
        tmp->left = tmp->right = nullptr;
        delete tmp;
        --num_elements;

        if (!empty() && tmp == header.left) {
            header.left = root()->minimal();
        }

        return pos;
    }

    iterator erase(const_iterator first, const_iterator last) {
        while (first != last) {
            first = erase(first);
        }
        return last;
    }

    size_type erase(const T &value) {
        if (empty()) {
            return 0;
        }
        set_root(find(root(), value));
        if (!comp.equiv(root(), value)) {
            return 0;
        }
        erase(iterator(root()));
        return 1;
    }

    iterator lower_bound(const T &value) {
        if (empty()) {
            return end();
        }
        set_root(find(root(), value));
        iterator res = iterator(root());
        if (res.node && comp(res.node, value)) {
            ++res;
        }
        return res;
    }

    iterator upper_bound(const T &value) {
        iterator res = lower_bound(value);
        if (res.node && comp.equiv(res.node, value)) {
            ++res;
        }
        return res;
    }

    iterator find(const T &value) {
        iterator res = lower_bound(value);
        if (res.node && comp.equiv(res.node, value)) {
            return res;
        }
        return end();
    }

    std::pair<iterator, iterator> equal_range(const T& value) {
        return std::make_pair(lower_bound(value), upper_bound(value));
    }
};

template <class T, class Compare>
class splay<T, Compare>::splay_node_comparator {
    Compare comp;
  public:
    splay_node_comparator(const Compare& comp_) : comp(comp_) {
    }

    splay_node_comparator(const splay_node_comparator& other) : comp(other.comp) {
    }

    splay_node_comparator& operator=(const splay_node_comparator& other) {
        comp = other.comp;
        return *this;
    }

    const T& cast(splay_node_base* a) {
        return static_cast<splay_node*>(a)->elem;
    }

    const T& cast(const T& a) {
        return a;
    }

    template<class Ta, class Tb>
    bool operator()(Ta a, Tb b) {
        return comp(cast(a), cast(b));
    }

    template<class Ta, class Tb>
    bool equiv(Ta a, Tb b) {
        return !operator()(a, b) && !operator()(b, a);
    }
};

template <class T, class Compare>
class splay<T, Compare>::splay_node_base {
    splay_node_base *go_max(bool dir) const {
        splay_node_base *res = const_cast<splay_node_base*>(this);

        while (res->s[dir]) {
            res = res->s[dir];
        }
        return res;
    }

  public:
    splay_node_base *s[2];
    splay_node_base *parent;

    virtual ~splay_node_base() {}

    splay_node_base *minimal() const {
        return go_max(0);
    }

    splay_node_base *maximal() const {
        return go_max(1);
    }

    /* return true if i'm right son or false if i'm left son */
    bool direction() const {
        return this->parent->right == this ? true : false;
    }

    bool is_root() const {
        return this->parent->parent == this;
    }

    splay_node_base *go_to(bool dir) {
        splay_node_base *p = this;
        splay_node_base *c = this;

        if (c->right == c) {
            if (dir || c->parent == nullptr) {
                return c;
            } else {
                return c->parent->maximal();
            }
        }

        while (true) {
            if (c->s[dir] && c->s[dir] != p) {
                c = c->s[dir];
                while (c->s[!dir]) {
                    c = c->s[!dir];
                }
                return c;
            }
            if (c->is_root()) {
                return c->parent;
            }
            if (c->direction() == dir) {
                p = c;
                c = c->parent;
            } else {
                return c->parent;
            }
        }
    }

    splay_node_base *next() {
        return go_to(1);
    }

    splay_node_base *prev() {
        return go_to(0);
    }

    /* my parent become my son, and my son became my grandson */
    splay_node_base *rotate() {
        bool dir = this->direction();
        splay_node_base *q = this->s[!dir];
        splay_node_base *p = this->parent;
        // setting my parent as my parent parent
        this->parent = p->parent;
        // if i'm not root, change my new parent son from my old parent to me
        if (!p->is_root()) {
            this->parent->s[p->direction()] = this;
        } else { // if i'm parent change haeder to point to me
            this->parent->parent = this;
        }
        // setting me as my parent and my parent as my son
        this->s[!dir] = p;
        p->parent = this;
        // setting my old son as my old parent son
        p->s[dir] = q;
        // if my old son exist, set his parent to my old parent (now my son)
        if (q != nullptr) {
            q->parent = p;
        }
        return this;
    }

    splay_node_base *splay() {
        while (!this->is_root()) {
            if (this->parent->is_root()) { // my parent is a root - Zig
                this->rotate();
            } else if (this->direction() == this->parent->direction()) { // Zig-Zig
                this->parent->rotate();
                this->rotate();
            } else { // Zig-Zag
                this->rotate();
                this->rotate();
            }
        }
        return this;
    }
};

template <class T, class Compare>
class splay<T, Compare>::splay_node : public splay_node_base {
  public:
    const T elem;

    splay_node(splay_node const&) = delete;
    splay_node & operator=(splay_node const&) = delete;
    splay_node(splay_node &&) = delete;
    splay_node & operator=(splay_node &&) = delete;

    splay_node(T value, splay_node_base *par) : elem(value) {
        this->parent = par;
        this->right = this->left = nullptr;
    }

    splay_node(splay_node_base *other) : elem(static_cast<splay_node*>(other)->elem) {
        this->parent = nullptr;
        this->right = this->left = nullptr;
        if (other->left) {
            this->left = new splay_node(other->left);
            this->left->parent = this;
        }
        if (other->right) {
            this->right = new splay_node(other->right);
            this->right->parent = this;
        }
    }

    ~splay_node() {
        if (this->left) {
            delete this->left;
        }
        if (this->right) {
            delete this->right;
        }
    }
};

#undef left
#undef right

template <class T, class Compare>
class splay<T, Compare>::const_iterator {
    splay<T, Compare>::splay_node_base *node;
  public:
    typedef T value_type;
    typedef const T* pointer;
    typedef const T& reference;
    typedef std::ptrdiff_t difference_type;
    typedef std::bidirectional_iterator_tag iterator_category;

    const_iterator() : node(nullptr) {}
    explicit const_iterator(splay<T, Compare>::splay_node_base *node_) : node(node_) {}
    ~const_iterator() {}

    const_iterator(const_iterator && other) : node(other.node) {}

    const_iterator & operator=(const_iterator && other) {
        node = other.node;
        return *this;
    }

    const_iterator(const_iterator const& other) : node(other.node) {}

    const_iterator & operator=(const_iterator const &other) {
        node = other.node;
        return *this;
    }

    const_iterator & operator++() {
        node = node->next();
        return *this;
    }

    const_iterator operator++(int) {
        const_iterator res = const_iterator(node);
        node = node->next();
        return res;
    }

    const_iterator & operator--() {
        node = node->prev();
        return *this;
    }

    const_iterator operator--(int) {
        const_iterator res = const_iterator(node);
        node = node->prev();
        return res;
    }

    bool operator==(const_iterator const& other) const {
        return node == other.node;
    }

    bool operator!=(const_iterator const& other) const {
        return node != other.node;
    }

    reference operator*() const {
        return static_cast<splay_node*>(node)->elem;
    }

    pointer operator->() const {
        return &static_cast<splay_node*>(node)->elem;
    }

    friend class splay<T, Compare>;
};
